# Python docstrings have type annotations

The docstrings for the Python wrappers contain type hints as described
in PEP 484.  This and other changes to the Python wrappers will improve
IDE tab completion and hinting.
